package com.example.dell.button_const;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Buttonmain extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buttonmain);

        Button button01 = (Button) findViewById(R.id.button_1st);
        final TextView text01 = (TextView) findViewById(R.id.text_11);

        button01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text01.setText("Good Morning");
            }
        });

        Button button02 = (Button) findViewById(R.id.button_2nd);
        final TextView text02 = (TextView) findViewById(R.id.text_11);


        button02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text02.setText("Good Noon");
            }
        });

        Button button03 = (Button) findViewById(R.id.button_3rd);
        final TextView text03 = (TextView) findViewById(R.id.text_11);

        button03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text03.setText("Good Night");
            }
        });
    }
}
